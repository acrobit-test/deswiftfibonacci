//
//  DEFibonacci.swift
//  DESwiftFibonacci
//
//  Created by Diego Espinoza on 30/01/23.
//

import Foundation

/// Struct that contains the implementation for calculating the fibonacci number of an input.
public class DEFibonacci {
    private var operationQueue = OperationQueue()
    private var operation: FibonacciOperation?
    
    public init() {
        operationQueue.qualityOfService = .background
    }
    
    /// Method for calculating the fibonacci number of the input Integer. The calculation is performed on a background operation queue to avoid blocking the main queue.
    ///
    /// - Parameters:
    ///     - number: The Integer which will have its fibonacci number calculated
    ///     - onCompletion: Callback that will return the result of the calculation. It return the value using the main queue.
    public func calculate(usingNumber number: Int,
                          onCompletion: @escaping (Int64) -> Void) {
        let operation = FibonacciOperation(number: number)
        operation.onCompletion { result in
            OperationQueue.main.addOperation {
                onCompletion(result)
            }
        }
        self.operation = operation
        operationQueue.addOperation(operation)
    }
    
    /// This method is used to cancel the calculation operation. The returned value in this case will be -1.
    
    public func cancelCalculation() {
        operation?.cancel()
    }
}
