//
//  DEFibonacciOperation.swift
//  DESwiftFibonacci
//
//  Created by Diego Espinoza on 2/02/23.
//

import Foundation

internal class FibonacciOperation: Operation {
    let number: Int
    var result: Int64 = 0
    private var _onCompletion: ((Int64) -> Void)?
    
    init(number: Int) {
        self.number = number
        super.init()
    }
    
    override func main() {
        var firstNumber: Int64 = 1
        var secondNumber: Int64 = 1
        
        guard number > 0 else {
            result = 0
            return
        }
        
        guard number > 1 else {
            result = 1
            return
        }
        
        for _ in 2..<number {
            if isCancelled {
                result = -1
                _onCompletion?(result)
                break
            }
            (firstNumber, secondNumber) = (firstNumber + secondNumber, firstNumber)
        }
        
        if !isCancelled {
            result = firstNumber
        }
    }
    
    func onCompletion(_ completion: @escaping (Int64) -> Void) {
        _onCompletion = completion
        self.completionBlock = { [weak self] in
            guard let owner = self else {
                completion(-1)
                return
            }
            completion(owner.result)
        }
    }
}
