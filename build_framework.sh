#!/bin/sh

rm -rf build
rm -rf DESwiftFibonacci.xcframework

xcodebuild archive \
-scheme DESwiftFibonacci \
-destination "generic/platform=iOS" \
-archivePath build/DESwiftFibonacci-iOS \
SKIP_INSTALL=NO \
BUILD_LIBRARY_FOR_DISTRIBUTION=YES

xcodebuild archive \
-scheme DESwiftFibonacci \
-destination "generic/platform=iOS Simulator" \
-archivePath build/DESwiftFibonacci-iOS-Sim \
SKIP_INSTALL=NO \
BUILD_LIBRARY_FOR_DISTRIBUTION=YES

xcodebuild -create-xcframework \
-framework build/DESwiftFibonacci-iOS.xcarchive/Products/Library/Frameworks/DESwiftFibonacci.framework \
-framework build/DESwiftFibonacci-iOS-Sim.xcarchive/Products/Library/Frameworks/DESwiftFibonacci.framework \
-output DESwiftFibonacci.xcframework