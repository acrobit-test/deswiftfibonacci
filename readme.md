# Swift Fibonacci

This is the source code for the implementation of the Fibonacci number calculation.

To call the method , first the `DEFibonacci` struct must be initialized and then the `calculate()` method needs to be called.

This `calculate()` method is performed in a background thread to avoid locking the main queue of the app. Once it finishes it returns the calculated number in a closure. This closure is already in the Main Queue.


## How to generate the Framework

1. Open Terminal
2. Go to the project folder
3. Run the script build_framework.sh
4. XCFramework folder should be generated in the source folder


## How to use this framework

The framework can be imported in two ways:

1. Directly copying the `XCFramework` output to the project.
2. Using Swift Package Manager and importing the library using the url: https://gitlab.com/acrobit-test/deswiftfibonaccipackage


## Testing

Some unit testing has been implemented, this will test that the calculated result by the implementation is actually the correct one.