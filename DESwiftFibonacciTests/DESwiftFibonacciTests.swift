//
//  DESwiftFibonacciTests.swift
//  DESwiftFibonacciTests
//
//  Created by Diego Espinoza on 31/01/23.
//

import XCTest
@testable import DESwiftFibonacci

final class DESwiftFibonacciTests: XCTestCase {

    var sut: DEFibonacci!
    
    override func setUpWithError() throws {
        sut = DEFibonacci()
    }

    func testNumberZero() {
        let expectation = expectation(description: "Calculation to finish")
        var result: Int?
        sut.calculate(usingNumber: 0) { value in
            result = Int(value)
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 5) { _ in
            XCTAssert(result == 0)
        }
    }
    
    func testNumberOne() {
        let expectation = expectation(description: "Calculation to finish")
        var result: Int?
        sut.calculate(usingNumber: 1) { value in
            result = Int(value)
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 5) { _ in
            XCTAssert(result == 1)
        }
    }
    
    func testNumberTwo() {
        let expectation = expectation(description: "Calculation to finish")
        var result: Int?
        sut.calculate(usingNumber: 2) { value in
            result = Int(value)
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 5) { _ in
            XCTAssert(result == 1)
        }
    }
    
    func testNumberThree() {
        let expectation = expectation(description: "Calculation to finish")
        var result: Int?
        sut.calculate(usingNumber: 3) { value in
            result = Int(value)
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 5) { _ in
            XCTAssert(result == 2)
        }
    }
    
    func testNumberFour() {
        let expectation = expectation(description: "Calculation to finish")
        var result: Int?
        sut.calculate(usingNumber: 4) { value in
            result = Int(value)
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 5) { _ in
            XCTAssert(result == 3)
        }
    }
    
    func testNumber80() {
        let expectation = expectation(description: "Calculation to finish")
        var result: Int?
        sut.calculate(usingNumber: 80) { value in
            result = Int(value)
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 5) { _ in
            XCTAssert(result == 23416728348467685)
        }
    }
    
    func testNumber50() {
        let expectation = expectation(description: "Calculation to finish")
        var result: Int?
        sut.calculate(usingNumber: 50) { value in
            result = Int(value)
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 5) { _ in
            XCTAssert(result == 12586269025)
        }
    }
    
    func testMultipleFibonacci() {
        let firstExpectation = expectation(description: "Calculation to finish")
        let secondExpectation = expectation(description: "Calculation to finish")
        let thirdExpectation = expectation(description: "Calculation to finish")
        
        var firstResult: Int?
        var secondResult: Int?
        var thirdResult: Int?
        
        sut.calculate(usingNumber: 9) { value in
            firstResult = Int(value)
            firstExpectation.fulfill()
        }
        
        sut.calculate(usingNumber: 8) { value in
            secondResult = Int(value)
            secondExpectation.fulfill()
        }
        
        sut.calculate(usingNumber: 7) { value in
            thirdResult = Int(value)
            thirdExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 10) { _ in
            guard let first = firstResult,
                  let second = secondResult,
                  let third = thirdResult
            else {
                XCTAssert(false, "Did not finish calculations")
                return
            }
            
            XCTAssert(first == second + third)
        }
    }
    
    func testMultipleFibonacciTwo() {
        let firstExpectation = expectation(description: "Calculation to finish")
        let secondExpectation = expectation(description: "Calculation to finish")
        let thirdExpectation = expectation(description: "Calculation to finish")
        
        var firstResult: Int?
        var secondResult: Int?
        var thirdResult: Int?
        
        sut.calculate(usingNumber: 12) { value in
            firstResult = Int(value)
            firstExpectation.fulfill()
        }
        
        sut.calculate(usingNumber: 11) { value in
            secondResult = Int(value)
            secondExpectation.fulfill()
        }
        
        sut.calculate(usingNumber: 10) { value in
            thirdResult = Int(value)
            thirdExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 10) { _ in
            guard let first = firstResult,
                  let second = secondResult,
                  let third = thirdResult
            else {
                XCTAssert(false, "Did not finish calculations")
                return
            }
            
            XCTAssert(first == second + third)
        }
    }
}
